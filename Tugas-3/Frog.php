<?php

class Frog extends Animal
{
  private $legs = 2;
  public function __construct($name)
  {
    parent::__construct($name);
  }

  public function jump()
  {
    return  'Hop hop!';
  }

  public function getLegs()
  {
    return $this->legs;
  }
}
