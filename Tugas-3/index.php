
<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Name: " . $sheep->get_name();
echo "<br>";
echo "Legs: " . $sheep->get_legs();
echo "<br>";
echo "cold blooded: " . $sheep->get_cold_blooded();
echo "<br>";
echo "<br>";

$kodok = new Frog('buduk');
echo "Name: " . $kodok->get_name();
echo "<br>";
echo "Legs: " . $kodok->getLegs();
echo "<br>";
echo "cold blooded: " . $kodok->get_cold_blooded();
echo "<br>";
echo "jump: " . $kodok->jump();
echo "<br>";
echo "<br>";

$sungokong = new Ape('kera sakti');
echo "Name: " . $sungokong->get_name();
echo "<br>";
echo "Legs: " . $sungokong->get_legs();
echo "<br>";
echo "cold blooded: " . $sungokong->get_cold_blooded();
echo "<br>";
echo "Yell: " . $sungokong->yell();
