<?php
function tukar_besar_kecil($string)
{
  $result = "";
  for ($i = 0; $i < strlen($string); $i++) {
    if (ctype_upper($string[$i])) {
      $result .= strtolower($string[$i]);
    } else {
      $result .= strtoupper($string[$i]);
    }
  }
  return $result;
}

// TEST CASES
// echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
// echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
// echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
// echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
// echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

function ubah_huruf($string)
{
  $result = "";
  for ($i = 0; $i < strlen($string); $i++) {
    $result .= chr(ord($string[$i]) + 1);
  }
  return $result;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
